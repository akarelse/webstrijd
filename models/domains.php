<?php
namespace Model;

use \Gas\Core;
use \Gas\ORM;

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Domains extends Prototype
{
    
    public $primary_key = 'id';
    
    
    function _init() {
        parent::_init();
        
        self::$relationships = array(
            'categories' => ORM::belongs_to('\\Model\\Categories'),
            'websites'   => ORM::has_many('\\Model\\Websites')
        );
        
        self::$fields = array(
            'id' => ORM::field('auto[10]') ,
            'name' => ORM::field('char[64]') ,
            'slug' => ORM::field('char[255]', array(
                'required'
            )),
            'domain' => ORM::field('char[255]', array(
                'required'
            )),
            'interval' => ORM::field('int[3]', array(
                'required'
            ))
        );
    }

    function categories_id_name()
    {
        $temp = array('None');
        foreach (Categories::all() as $value) {
            $temp[$value->id] = $value->name;
        }
        return $temp;
    }
    
    function _before_save() {
        
        
        return $this;
    }

    function _before_check()
    {

        if ($this->record['data']['categories_id'] == 0)
        {
            $this->errors[] = "LOL";
        }

        $fields = $this->remove_non_fields($this->record->get('data'));
        $this->record->set('data',$fields);
        return $this;
    }




}
