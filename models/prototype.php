<?php
namespace Model;

use \Gas\Core;
use \Gas\ORM;

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Prototype extends Orm
{
	protected $lists;
	
	function _init() {
		$this->lists = new \StdClass();
	}
	
	function is_required($field) {
		if (array_key_exists($field, $this->meta['fields'])) {
			if (isset($this->meta['fields'][$field]['rules'])) {
				return (strpos($this->meta['fields'][$field]['rules'], 'required') !== false);
			}
		}
		return false;
	}
	
	function remove_non_fields($all) {

		$temp = array();
		$include = array_keys($this->meta['fields']);
		foreach ($this->meta['entities'] as $value) {
			if ($value['type'] == 'belongs_to') {
				$tempo = explode('\\', $value['path']);
				$include[] = strtolower(end($tempo)) . "_id";
			}
		}
		foreach ($all as $key => $value) {
			if (in_array($key, $include)) {
				$temp[$key] = $value;
			}
		}
		return $temp;
	}
	
	function fill_all($post) {
		foreach ($post as $key => $value) {
			$this->record->set('data.' . $key, $value);
		}
	}
	
	function make_extra_data($all_post) {
		$this->make_correct_oa($all_post);
		$all_post = gzcompress(serialize((object)$all_post) , 9);
		return $all_post;
	}
	
	function make_correct_oa(&$item) {
		if (is_array($item) || is_object($item)) {
			foreach ($item as $key => & $value) {
				if ($key == 0)
				{
					unset($item[0]);	
				}
				$this->make_correct_oa($value);
				if (!is_numeric($key) && (is_array($value) || is_object($value))) {
					$newkey = plural($key);
					unset($item[$key]);
					$item[$newkey] = $value;
				}
				if (is_numeric($key)) {
					$value = (object)$value;
				}
			}
		}
	}
	
	function preg_grep_keys($pattern, $input, $flags = 0) {
		$keys = preg_grep($pattern, array_keys($input) , $flags);
		$vals = array();
		foreach ($keys as $key) {
			$vals[$key] = $input[$key];
		}
		return $vals;
	}
	
	function merge_extra_data($extra_data, $coupled, $special = false) {
		if (!array_key_exists($extra_data, $this->meta['fields'])) {
			return array();
		}
		
		$extra = unserialize(gzuncompress($this->{$extra_data}));
		return $extra;
		
	}
	
	public function deletable($dependant) {
		
		if (array_key_exists($dependant, $this->meta['entities'])) {
			$totest = $this->{$dependant}();
			return empty($totest);
		}
		return false;
	}
	
	public function save_delete($dependant) {
		
		if (array_key_exists($dependant, $this->meta['entities'])) {
			$totest = $this->{$dependant}();
			if ($save = (empty($totest))) {
				$this->delete();
			}
			return $save;
		}
		return false;
	}
}
