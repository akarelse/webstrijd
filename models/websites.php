<?php
namespace Model;

use \Gas\Core;
use \Gas\ORM;

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Websites extends Prototype
{
    
    public $primary_key = 'id';
    
    
    function _init() {
        parent::_init();

  
        
        self::$relationships = array(
            'domains' => ORM::belongs_to('\\Model\\Domains')
        );
        
        self::$fields = array(
            'id' => ORM::field('auto[10]') ,
            'name' => ORM::field('char[64]', array(
                'required'
            )) ,
            'slug' => ORM::field('char[255]') ,
            'type_id' => ORM::field('int[2]', array(
                'required'
            )) ,
            'redirects' => ORM::field('int[2]', array(
                'required'
            )) ,
            'compression' => ORM::field('int[1]', array(
                'required',
            )) ,
            'no_css' => ORM::field('int[2]',array(
                'required',
            )) ,
            'no_js' => ORM::field('int[2]',array(
                'required',
            )) ,
            'cache_for' => ORM::field('int[6]',array(
                'required',
            )) ,
            'time' => ORM::field('int[3]',array(
                'required',
            ))
        );

        $this->ts_fields = array('[time_created]');
    }

    
    function _before_save() {

        return $this;
    }

    function _before_check()
    {

        return $this;
    }


}
