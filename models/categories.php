<?php
namespace Model;

use \Gas\Core;
use \Gas\ORM;

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Categories extends Prototype
{
    
    public $primary_key = 'id';
    
    
    function _init() {
        parent::_init();

        self::$relationships = array(
            'domains' => ORM::has_many('\\Model\\Domains')
        );
        
        self::$fields = array(
            'id' => ORM::field('auto[10]') ,
            'name' => ORM::field('char[64]', array(
                'required'
            )) ,
            'slug' => ORM::field('char[255]')
        );
    }

    function _before_save() {
        return $this;
    }

    function _before_check()
    {
        $fields = $this->remove_non_fields($this->record->get('data'));
        $this->record->set('data',$fields);
        return $this;
    }


}
