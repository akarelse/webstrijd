<?php
//messages
$lang['webstrijd:success']			=	'It worked';
$lang['webstrijd:error']			=	'It didn\'t work';
$lang['webstrijd:no_items']			=	'No Items';

//page titles
$lang['webstrijd:create']			=	'Create Item';

//labels
$lang['webstrijd:name']				=	'Name';
$lang['webstrijd:slug']				=	'Slug';
$lang['webstrijd:manage']			=	'Manage';
$lang['webstrijd:item_list']		=	'Item List';
$lang['webstrijd:view']				=	'View';
$lang['webstrijd:edit']				=	'Edit';
$lang['webstrijd:delete']			=	'Delete';

//buttons
$lang['webstrijd:custom_button']	=	'Custom Button';
$lang['webstrijd:items']			=	'Items';
?>