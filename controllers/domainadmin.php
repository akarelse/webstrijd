<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Domainadmin extends Admin_Controller
{
	protected $section = 'domains';
	
	public function __construct() {
		parent::__construct();
		
		// Load all the required classes
		$this->load->library('gas');
		$this->lang->load('gas');
		$this->load->library('form_validation');
		$this->lang->load('webstrijd');
		
		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')->append_css('module::admin.css');
	}
	
	/**
	 * List all items
	 */
	public function index() {
		$data = new stdClass();
		$data->domains = Model\Domains::all();
		$this->template->title($this->module_details['name'])->build('admin/domains/items', $data);
	}
	
	public function create() {
		$data = new stdClass();
		$data->domain = new Model\Domains();
		
		if ($input = $this->input->post()) {
			$data->domain->fill_all($input);
			if ($data->domain->save(TRUE)) {
				
				$this->session->set_flashdata('success', lang('equipment:success'));
				if ($input['btnAction'] == 'save_exit') {
					redirect('admin/webstrijd/domainadmin');
				}
				redirect('admin/webstrijd/domainadmin/edit/' . $id);
			}
		}
		
		$this->template->title($this->module_details['name'], lang('webstrijd.new_item'))->build('admin/domains/form', $data);
	}
	
	public function edit($id = 0) {
		$data = new stdClass();
		$data->domain = Model\Domains::find($id);
		if ($input = $this->input->post()) {
			$data->domain->fill_all($input);
			if ($data->domain->save(TRUE)) {
				
				$this->session->set_flashdata('success', lang('equipment:success'));
				if ($input['btnAction'] == 'save_exit') {
					redirect('admin/webstrijd/domainadmin');
				}
				redirect('admin/webstrijd/domainadmin/edit/' . $id);
			}
		}
		
		$this->template->title($this->module_details['name'], lang('webstrijd.edit'))->build('admin/domains/form', $data);
	}
	
	/**
	 * the controller delete method, deletes a period
	 * @param type $id
	 * @return void
	 */
	public function delete($id = 0) {
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
			$temp = array();
			foreach ($this->input->post('action_to') as $value) {
				$temp[] = Model\Domains::delete($value);
			}
			if (!in_array(false, $temp)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/domainadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		} else {
			if (Model\Domains::delete($id)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/domainadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		}
		redirect('admin/webstrijd/domainadmin');
	}
}
