<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Websiteadmin extends Admin_Controller
{
	
	// protected $section = 'websites';
	
	public function __construct() {
		parent::__construct();
		
		// Load all the required classes
		$this->load->library('gas');
		$this->lang->load('gas');
		$this->load->library('form_validation');
		$this->lang->load('webstrijd');
		
		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')->append_css('module::admin.css');
	}
	
	/**
	 * List all items
	 */
	public function index() {
		$data = new stdClass();
		$data->websites = Model\Websites::all();
		$this->template->title($this->module_details['name'])->build('admin/websites/items', $data);
	}
	
	public function delete($id = 0) {
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
			$temp = array();
			foreach ($this->input->post('action_to') as $value) {
				$temp[] = Model\Websites::delete($value);
			}
			if (!in_array(false, $temp)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/websiteadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		} else {
			if (Model\Websites::delete($id)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/websiteadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		}
		redirect('admin/webstrijd/websiteadmin');
	}
}
