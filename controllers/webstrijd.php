<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Webstrijd extends Public_Controller
{
	public function __construct() {
		parent::__construct();
		
		// Load the required classes
		$this->lang->load('webstrijd');
		$this->load->library('gas');
		$this->lang->load('gas');
		
		$this->template->append_css('module::webstrijd.css')->append_js('module::webstrijd.js');
	}
	
	/**
	 * All items
	 */
	public function index($offset = 0) {
		$data = new stdClass();
		
		$this->template->title($this->module_details['name'], 'the rest of the page title')->build('index', $data);
	}
	
	public function next_to_crawl() {
		if ($this->get_client_ip() == "127.0.0.1") {
			$websites = Model\Websites::order_by('time_created', 'asc')->group_by('domains_id')->all();
			$domains = array();
			foreach ($websites as $value) {
				$lastdate = new DateTime($value->time_created);
				$interval = new DateInterval('PT' . $value->domains()->interval . 'H');
				$lastdate->add($interval);
				$now = new DateTime();
				
				if ($lastdate < $now) {
					$domains[] = $value->domains()->domain;
				}
			}
			if (!empty($domains)) {
				print json_encode($domains);
				return;
			}
			print "NOTHING TO DO !";
			return;
		}
		print "NO ACCESS !";
		return;
		
		//als update time verstreken
		//dan geef results in vorm of json
		
	}
	
	public function post_result() {
		
		//do check localhost
		//process json.
		
	}
	
	function get_client_ip() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			 //check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			 //to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}
