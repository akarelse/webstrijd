<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Categorieadmin extends Admin_Controller
{
	protected $section = 'categories';
	
	public function __construct() {
		parent::__construct();
		
		// Load all the required classes
		
		$this->load->library('gas');
		$this->lang->load('gas');
		$this->load->library('form_validation');
		$this->lang->load('webstrijd');
		
		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')->append_css('module::admin.css');
	}
	
	/**
	 * List all items
	 */
	public function index() {
		$data = new stdClass();
		$data->categories = Model\Categories::all();
		$this->template->title($this->module_details['name'])->build('admin/categories/items', $data);
	}
	
	public function create() {
		$data = new stdClass();
		$data->categorie = new Model\Categories();
		if ($input = $this->input->post()) {
			$data->categorie->fill_all($input);
			if ($data->categorie->save(TRUE)) {
				$this->session->set_flashdata('success', lang('equipment:success'));
				if ($input['btnAction'] == 'save_exit') {
					redirect('admin/webstrijd/categorieadmin');
				}
				redirect('admin/webstrijd/categorieadmin/edit/' . $id);
			}
		}
		
		$this->template->title($this->module_details['name'], lang('webstrijd.new_item'))->build('admin/categories/form', $data);
	}
	
	public function edit($id = 0) {
		$data = new stdClass();
		$data->categorie = Model\Categories::find($id);
		if ($data->categorie->save(TRUE)) {
				$this->session->set_flashdata('success', lang('equipment:success'));
				if ($input['btnAction'] == 'save_exit') {
					redirect('admin/webstrijd/categorieadmin');
				}
				redirect('admin/webstrijd/categorieadmin/edit/' . $id);
			}
		$this->template->title($this->module_details['name'], lang('webstrijd.edit'))->build('admin/categories/form', $data);
	}
	
	public function delete($id = 0) {
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
			$temp = array();
			foreach ($this->input->post('action_to') as $value) {
				$temp[] = Model\Categories::delete($value);
			}
			if (!in_array(false, $temp)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/categorieadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		} else {
			if (Model\Categories::delete($id)) {
				$this->session->set_flashdata('success', lang('webstrijd:success'));
				redirect('admin/webstrijd/categorieadmin');
			}
			$this->session->set_flashdata('error', lang('webstrijd:some_undeleted'));
		}
		redirect('admin/webstrijd/categorieadmin');
	}
}
