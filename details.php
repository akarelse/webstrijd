<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_Webstrijd extends Module
{
	
	public $version = '1.0';
	const MIN_PHP_VERSION = '5.2.0';
	const MIN_PYROCMS_VERSION = '2.1';
	const MAX_PYROCMS_VERSION = '2.2.3';
	
	public function info() {
		return array(
			'name' => array(
				'en' => 'Webstrijd'
			) ,
			'description' => array(
				'en' => 'This is a PyroCMS module webstrijd.'
			) ,
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content',
			 // You can also place modules in their top level menu. For example try: 'menu' => 'Webstrijd',
			'sections' => array(
				'websites' => array(
					'name' => 'webstrijd:websites',
					 // These are translated from your language file
					'uri' => 'admin/webstrijd/websiteadmin',
					'shortcuts' => array(
						'create' => array(
							'name' => 'websites:create-website',
							'uri' => 'admin/webstrijd/websiteadmin/create',
							'class' => 'add'
						)
					)
				) ,
				
				'domains' => array(
					'name' => 'webstrijd:domains',
					 // These are translated from your language file
					'uri' => 'admin/webstrijd/domainadmin',
					'shortcuts' => array(
						'create' => array(
							'name' => 'webstrijd:creat-domain',
							'uri' => 'admin/webstrijd/domainadmin/create',
							'class' => 'add'
						)
					)
				) ,
				
				'categories' => array(
					'name' => 'webstrijd:categories',
					 // These are translated from your language file
					'uri' => 'admin/webstrijd/categorieadmin',
					'shortcuts' => array(
						'create' => array(
							'name' => 'webstrijd:create-categorie',
							'uri' => 'admin/webstrijd/categorieadmin/create',
							'class' => 'add'
						)
					)
				)
			)
		);
	}
	
	public function install() {
		if (!$this->check_php_version()) {
			$this->session->set_flashdata('error', 'Need a higher php version !');
			return FALSE;
		}
		
		if (!$this->check_pyrocms_version()) {
			$this->session->set_flashdata('error', 'Not the right PyroCMS version !');
			return FALSE;
		}
		
		$domains = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			) ,
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'domain' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'categories_id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'default' => 0,
                'null' => true
			),
			'interval' => array(
				'type' => 'INT',
				'constraint' => '3',
			)
		);
		
		$websites = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			) ,
			'domains_id' => array(
				'type' => 'INT',
				'constraint' => '11'
			) ,
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'redirects' => array(
				'type' => 'INT',
				'constraint' => '11',
			) ,
			'compression' => array(
				'type' => 'TINYINT',
				'constraint' => '1',
			) ,
			'no_css' => array(
				'type' => 'INT',
				'constraint' => '1',
			) ,
			'no_js' => array(
				'type' => 'INT',
				'constraint' => '1',
			) ,
			'cache_for' => array(
				'type' => 'INT',
				'constraint' => '6',
			) ,
			'size' => array(
				'type' => 'INT',
				'constraint' => '6',
			) ,
			'time' => array(
				'type' => 'INT',
				'constraint' => '3',
			),
			'time_created' => array(
				'type' => 'TIMESTAMP'
			)
		);
		
		$categories = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			) ,
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			) ,
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			)
		);
		
		$this->db->trans_start();
		
		$this->dbforge->add_field($domains);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('domains');
		
		$this->dbforge->add_field($websites);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('websites');
		
		$this->dbforge->add_field($categories);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('categories');
		
		$this->settings('add');
		
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function uninstall() {
		if ($this->dbforge->drop_table('domains') && $this->dbforge->drop_table('websites') && $this->dbforge->drop_table('categories')) {
			return TRUE;
		}
		return FALSE;
	}
	
	public function upgrade($old_version) {
		
		// Your Upgrade Logic
		return TRUE;
	}
	
	public function help() {
		
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	/**
	 * Check the current version of PHP and thow an error if it's not good enough
	 *
	 * @access private
	 * @return boolean
	 * @author Victor Michnowicz
	 */
	private function check_php_version() {
		if (version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0) {
			show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
			return FALSE;
		}
		return TRUE;
	}
	
	private function check_pyrocms_version() {
		if (version_compare(CMS_VERSION, self::MIN_PYROCMS_VERSION) < 0 && version_compare(CMS_VERSION, self::MAX_PYROCMS_VERSION) > 0) {
			show_error('This addon requires PYROCMS version ' . self::MIN__VERSION . ' minnimal or version ' . MAX_PYROCMS_VERSION . 'maximal.');
			return FALSE;
		}
		return TRUE;
	}
	
	public function settings($action, $items = array()) {
		
		// Variables
		$return = TRUE;
		$settings = array();
		$current = array();
		
		// Get existing settings
		foreach ($this->db->get('settings')->result_array() as $result) {
			$current[] = $result['slug'];
		}
		
		// Perform
		foreach ($settings as $setting) {
			
			if ((!empty($items) and in_array($setting['slug'], $items)) or empty($items)) {
				
				// Add it
				if ($action == 'add' and !in_array($setting['slug'], $current)) {
					
					if (!$this->db->insert('settings', $setting)) {
						$return = FALSE;
					}
					
					// Remove it
					
					
				} elseif ($action == 'remove' and in_array($setting['slug'], $current)) {
					
					if (!$this->db->delete('settings', array(
						'slug' => $setting['slug']
					))) {
						$return = FALSE;
					}
				}
			}
		}
		
		return $return;
	}
}

/* End of file details.php */
