<section class="title">
	<h4><?php echo lang('webstrijd:item_list'); ?></h4>
</section>

<section class="item">
	<?php echo form_open('admin/webstrijd/categorieadmin/delete');?>
	
	<?php if (!empty($categories)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('webstrijd:name'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $categories as $categorie ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $categorie->id); ?></td>
					<td><?php echo $categorie->name; ?></td>
					<td class="actions">
						<?php echo
						anchor('sample', lang('webstrijd:view'), 'class="button" target="_blank"').' '.
						anchor('admin/webstrijd/categorieadmin/edit/'.$categorie->id, lang('webstrijd:edit'), 'class="button"').' '.
						anchor('admin/webstrijd/categorieadmin/delete/'.$categorie->id, 	lang('webstrijd:delete'), array('class'=>'button')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('webstrijd:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
</section>