<section class="title">
	<h4><?php echo lang('webstrijd:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('webstrijd:name'); ?><?php if ($categorie->is_required('name')) { ?><span>*</span><?php } ?></label>
				<div class="input"><?php echo form_input('name', set_value('name', $categorie->name), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="slug"><?php echo lang('webstrijd:slug'); ?><?php if ($categorie->is_required('slug')) { ?></label>
				<div class="input"><?php echo form_input('slug', set_value('slug', $categorie->slug), 'class="width-15"'); ?></div>
			</li>
		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>
