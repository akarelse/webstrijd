<section class="title">
	<h4><?php echo lang('webstrijd:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('webstrijd:name'); ?><?php if ($domain->is_required('name')) { ?><span>*</span><?php } ?></label>
				<div class="input"><?php echo form_input('name', set_value('name', $domain->name), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="slug"><?php echo lang('webstrijd:slug'); ?><?php if ($domain->is_required('slug')) { ?><span>*</span><?php } ?></label>
				<div class="input"><?php echo form_input('slug', set_value('slug', $domain->slug), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="domain"><?php echo lang('webstrijd:domain'); ?><?php if ($domain->is_required('domain')) { ?><span>*</span><?php } ?></label>
				<div class="input"><?php echo form_input('domain', set_value('domain', $domain->domain), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="interval"><?php echo lang('webstrijd:interval'); ?><?php if ($domain->is_required('interval')) { ?><span>*</span><?php } ?></label>
				<div class="input"><?php echo form_input('interval', set_value('interval', $domain->interval), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="categorie"><?php echo lang('webstrijd:categorie'); ?> <span>*</span></label>
				<div class="input"><?php echo form_dropdown('categories_id', $domain->categories_id_name(), $domain->categories_id) ?></div>
			</li>
		</ul>
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>
